import axios from 'axios';
export class NocoDB {
    constructor(endpoint, mail, password) {
        this.endpoint = endpoint;
        this.apiKey = null;
        this.mail = mail;
        this.password = password;
    }
    async connect() {
        if (!this.apiKey) {
            await axios.post(`${this.endpoint}/api/v1/auth/user/signin`, {
                "email": this.mail,
                "password": this.password
            }).then((response) => {
                this.apiKey = response.data.token;
            }).catch((error) => {
                console.error(error);
            });
        }else {
            return this.apiKey;
        }
    }

    async get(url) {
        return await this.connect().then(async (response) => {
            const options = {
                method: 'GET',
                url: url,
                headers: {
                    'xc-auth': this.apiKey
                }
            };
            try {
                const response = await axios.request(options);
                return response.data;
            } catch (error) {
                console.error(error);
            }
        });
    }

      async get_table(org, projectName, tableName) {
          return await this.get(`${this.endpoint}/api/v1/db/data/${org}/${projectName}/${tableName}`);
      }
}